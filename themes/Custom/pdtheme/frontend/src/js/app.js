import css from '../css/style.scss';
import $ from 'jquery';
import slick from 'slick-carousel';
import validate from 'jquery-validation';
import selectbox from 'selectbox';
require('fancybox')($);

function app() {

    "use strict";

    // detect IE
    if (navigator.userAgent.match(/MSIE/img) || navigator.userAgent.match(/\.NET/img)) {
      document.body.classList.add('msie');
    }

    // iframe height
  (function() {

    let iframe = $('.header ~ iframe');

    function setFrameHeight(iframeSelector) {

      if (!iframeSelector.length) {
        throw new ReferenceError('Iframe required!');
      }

      let
        iframeContent = iframeSelector[0].contentWindow.document.documentElement.querySelector('.wrapper'),
        iframeNewHeight = iframeContent.getBoundingClientRect().height;

      return iframeSelector.height(iframeNewHeight);

    }

    if (iframe.length) {

      $(window).on('load resize', setFrameHeight(iframe));

    }

  })();

    // navigation
    $('#globalnav').on('change', e => $('html').toggleClass('static'));

    // article gallery
    $('.news-full').find('img').each(function() {
      const t = $(this), href = t.attr('src');
      t.wrap(`<a href="${href}" data-fancybox-group="gallery"></a>`);
    });

    // reviews slider
    $('.reviews').slick({
        dots: true,
        arrows: false,
        appendDots: $('.dots-reviews')
    });

    // photos slider

  (function() {
    const coachPhotos = $('.photos-coaches');
    coachPhotos.find('.photos').slick({
      autoplay: true,
      autoplaySpeed: 3000,
      pauseOnHover: true,
      prevArrow: coachPhotos.find('.slick-prev'),
      nextArrow: coachPhotos.find('.slick-next'),
      mobileFirst: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      responsive: [
        {
          breakpoint: 750,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3
          }
        }
      ]
    });
  })();

    // plans slider
    $('.plan').each(function() {
        const t = $(this), planBase = $('.plan-base');
        t.on('mouseenter', function() {
            t.addClass('plan-active')
                .siblings()
                .removeClass('plan-active');
            if (t.hasClass('plan-intense')) planBase.addClass('plan-muted');
            else planBase.removeClass('plan-muted');
        });
    });

      $('.gallery').slick({
        arrows: false,
        autoplay: false,
        speed: 500,
        dots: true,
        appendDots: $('.gallery-dots'),
        pauseOnDotsHover: true,
        customPaging: function (slider, i) {
          const bg = $(slider.$slides[i]).prop('href');
          return `<button class="gallery-dot" style="background-image: url(${bg})"></button>`;
        }
      });

    // selectbox
    function selectBox() {
      $('select').selectbox({
        isResponsive: false
      });
    }

    selectBox();

    // observe to the filter form

    (() => {

      if (window.Drupal) {

        Drupal.behaviors.playdrums = {
          attach: function(context, settings) {
            selectBox();
          }
        };

      }

    })();

    $('body').on('change', 'select', function(e) {
      if ($(this).closest('.filter').length) $(this).closest('form').find('[type="submit"]').trigger('click');
    });

    // form validation
    $('[novalidate]').each(function() {

      const t = $(this);
      let options = { rules: {}, messages: {} };

      t.find('[required]').each(function() {
        const t = $(this),
          name = t.attr('name');

        options.rules[name] = { required: true };
        options.messages[name] = { required: "Обязательное поле" };

        if (t.prop('type') === 'email') {
          options.rules[name].email = true;
          options.messages[name].email = "Пожалуйста, введите корректный e-mail";
        }

        if (t.attr('minlength')) {
          options.messages[name].minlength = $.validator.format("Необходимо ввести как минимум {0} символов");
        }

      });

      t.validate(options);

    });

    // input file
    // $('#user_file').each(function() {
    //   let t = $(this),
    //       input = t[0],
    //       form = t.closest('.form');
    //   input.addEventListener('change', function() {
    //     form.removeClass('loaded').addClass('loading');
    //     if (input.files.length) {
    //       let reader = new FileReader();
    //       reader.readAsDataURL(input.files[0]);
    //       reader.onload = function(e) {
    //         setTimeout(function() {
    //           form.removeClass('loading').addClass('loaded');
    //           $('.label-photo').html(`<img src="${e.target.result}" />`);
    //         }, 1000);
    //       };
    //     } else {
    //       setTimeout(function() {
    //         form.removeClass('loading').removeClass('loaded');
    //         $('.label-photo').html('');
    //       }, 1000);
    //     }
    //   });
    // });

  // fancybox
  $('[data-fancybox-group]').fancybox({
    padding: 0
  });

}

window.addEventListener('load', app);
