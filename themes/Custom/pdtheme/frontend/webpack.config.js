const
    path = require('path'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ExtractTextWebpackPlugin = require('extract-text-webpack-plugin'),
    OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin'),
    isProd = process.env.NODE_ENV === 'production',
    options = {
      entry: './src/js/app.js',
      output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/app.js'
      },
      module: {
        rules: [
          {
            test: /\.scss$/,
            use: ExtractTextWebpackPlugin.extract({
              fallback: 'style-loader',
              use: ['css-loader', 'postcss-loader', 'sass-loader'],
              publicPath: '../'
            })
          },
          // es6 to es5
          {
            test: /\.js$/,
            exclude: /node_modules/,
            use: 'babel-loader'
          },
          {
            test: /\.pug$/,
            use: [
              {
                loader: 'pug-loader',
                options: {
                  pretty: isProd
                }
              }
            ]
          },
          {
            test: /\.(jpe?g|png|gif|svg)$/i,
            use: [
              {
                loader: 'file-loader',
                options: {
                  name: '[path][name].[ext]',
                  outputPath: 'img/',
                  context: './src/img'
                }
              },
              'image-webpack-loader?bypassOnDebug'
            ]
          },
          {
            test: /\.(woff2?|eot|ttf)$/,
            use: 'file-loader?name=fonts/[name].[ext]'
          }
        ]
      },
      devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        hot: true,
        stats: 'errors-only',
        publicPath: '/'
      },
      plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
          title: 'Home',
          minify: false,
          favicon: './src/img/favicon.png',
          template: './src/index.pug',
          filename: 'index.html'
        }),
        new HtmlWebpackPlugin({
          title: 'Shop',
          minify: false,
          template: './src/shop.pug',
          filename: 'shop.html'
        }),
        new HtmlWebpackPlugin({
          title: 'Product',
          minify: false,
          template: './src/product.pug',
          filename: 'product.html'
        }),
        new HtmlWebpackPlugin({
          title: 'About',
          minify: false,
          template: './src/about.pug',
          filename: 'about.html'
        }),
        new HtmlWebpackPlugin({
          title: 'Contact',
          minify: false,
          template: './src/contact.pug',
          filename: 'contact.html'
        }),
        new HtmlWebpackPlugin({
          title: 'Testimonials',
          minify: false,
          template: './src/testimonials.pug',
          filename: 'testimonials.html'
        }),
        new HtmlWebpackPlugin({
          title: 'Teachers',
          minify: false,
          template: './src/teachers.pug',
          filename: 'teachers.html'
        }),
        new HtmlWebpackPlugin({
          title: 'Teacher',
          minify: false,
          template: './src/teacher.pug',
          filename: 'teacher.html'
        }),
        new HtmlWebpackPlugin({
          title: 'Media',
          minify: false,
          template: './src/media.pug',
          filename: 'media.html'
        }),
        new HtmlWebpackPlugin({
          title: 'News Full',
          minify: false,
          template: './src/news.pug',
          filename: 'news.html'
        }),
        new HtmlWebpackPlugin({
          title: 'Homepage',
          minify: false,
          template: './src/home.pug',
          filename: 'home.html'
        }),
        new HtmlWebpackPlugin({
          title: 'Страница не найдена',
          minify: false,
          template: './src/404.pug',
          filename: '404.html'
        }),
        new ExtractTextWebpackPlugin({
          filename: 'css/style.css',
          //disable: !isProd
        }),
        new OptimizeCssAssetsPlugin()
      ]
    };

module.exports = options;
